# Notes on Sales

Quick summary of a couple sales articles I've read.

## tl;dr;

1.   communication with customers and potential customers lets you get **actionable** feedback about their pain points with their current solution **so you can fix them in your product** — get feedback about pain points even before trying to make a sale
2.   you can **saturate small niches** much more easily than a entire market — start marketing to a small niche (i.e. a couple hundred potential customers) and then expand to neighboring ones
3.   let all your marketing efforts tie in to and reinforce each other so that no effort is wasted — a "marketing flywheel"

## Direct Sales for Startups

(summary of https://nathanbarry.com/sales/)

-   word of mouth doesn't work if you don't have enough customers
-   communication gives valuable feedback, unlike failed Google searches or trials
-   ask about frustrations to start the conversation
    -   "Is anything frustrating you with ______? We make ______, and we'd like to build it to better serve organizations like yours."
    -   start a conversation
-   lead generation: start with small niches
    -   tools like Nerdy Data or BuiltWith to find sites using competitors
    -   search for, say, all independent pet stores within 60 miles of Fargo
    -   creates an echo chamber, since they'll tend to talk to each other
-   once you saturate a niche, go to neighboring — and still small — niches
-   remove the biggest objection: lowers churn and more likely to get a sale
-   provide value to leads to continue conversation
-   travel — and make as many connections as you can when you do

## Why Marketing Flywheels Work

(summary of https://sparktoro.com/blog/why-marketing-flywheels-work/)

#### What is a flywheel?

If a marketing practice has the following characteristics:

-   a consistently-repeatable tactic (i.e. write an article, post to social media, buy an ad, etc.),
-   a system for either reaching a larger audience or decreasing the cost to reach each customer,
-   scalability (each bit of added effort makes things easier)

“…it creates a flywheel effect. You put in the same amount of work (or less) each time, and get more and more out of it the more you repeat it. This is a beautiful, highly-rewarding system. But, it usually takes **a long time and a lot of effort** to get going.”

Otherwise, **every single customer** takes the same amount of work to bring on. Worse, if your competitors find ways to improve their efficiency, **your** job gets harder. 

#### What to do?

The article lists several examples of successful campaigns, but of course those are not the only options. An example of a “Press & Word of Mouth” flywheel (note that it’s a cycle, so you all these stages affect each other — you don’t need to start at #1)

1.  attract customers who are happy to tell others about you
2.  reward loyal customers, especially for those who advertise for you
3.  generate industry news (such as with a new feature, service, or press release)
4.  boost the news with social media and paid ads

You can see how each of these steps plays off of the other to increase its effect. The whole is greater than the sum of its parts.

In general:

-   Pick one (free?) avenue that you want to try (blog, social media posts, etc.).
-   Make sure that the path from that avenue to a lead is clear and optimal. (i.e. clear call to action, no distractions from goal)
-   See if there are other (free?) channels that make sense, such as email newsletters or social media or guest posting blogs.
-   (You may need to experiment to find an avenue that works for you.)
-   When happy with the flow, buy ads to promote the avenue that is *already* succeeding.

It will take time to find avenues that work.

#### Website Traffic and Email Subscribers are most important

"Every channel and every tactic should either send you website traffic that can engage, click, and convert on your site OR send you email subscribers. Anything else is a huge waste."

I think the logic behind this is that you are trying to make sure you are getting value for everything you do. Silly to tell someone about us and not get their email address, if we can help it.

## How to get your first 10 customers

(summary of http://danshipper.com/nothing-happens-until-the-sale-is-made)

-   AdWords, blog posts, press releases, etc. don’t give you **actionable information** (i.e. how to answer the following questions).
-   You need answers to the following questions:
    -   **Where’s the value of my product?**
    -   **Who is it valuable for?**
    -   **Why is it valuable for them?**
    -   You **cannot assume** the answer to those questions. (You need to **know**.)
    -   You need to **talk to people** to find the answer.
    -   Never leave a call without establishing **clear next steps**/commitments.
    -   **Every time you make a sale, you understand a bit more about your product.**
    -   http://www.hoovers.com/sales-leads.html can be used for discovering leads.
-   The article is primarily focused on starting a new product, but I wonder if it also applies to:
    -   learning how to position our product better in the market
    -   talking to customers different than our core market